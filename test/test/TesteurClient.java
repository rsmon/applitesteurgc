
package test;


import fabrique.entite.client.FabEntiteClient;
import static junit.framework.Assert.*;
import org.junit.Before;
import org.junit.Test;
import webservice.soap.initbase.ServiceInitBase;
import webservice.soap.initbase.ServiceSoapInitBase;
import webservice.soap.pourtest.client.Client;
import webservice.soap.pourtest.client.Region;
import webservice.soap.pourtest.client.WebServiceTesteurClient;
import webservice.soap.pourtest.client.WebServiceTesteurClient_Service;


/**
 *
 * @author rsmon
 */
public class TesteurClient {
 
    WebServiceTesteurClient_Service serviceTesterClient;
   WebServiceTesteurClient          portTesterClient;
    
    @Before
    public void setUp() {
        
       reinitialiserBase();
              
       serviceTesterClient = new WebServiceTesteurClient_Service();
       portTesterClient    = serviceTesterClient.getWebServiceTesteurClientPort();
    }
    
    @Test
    public void testCaCLient101() {
    
    float ca=portTesterClient.getCaAnnuelClient(101L);
    
    assertEquals("Erreur ", 1721.0f, ca, 0.0f);
    
    }

   @Test
    public void testRechercheClient101(){
    
       Client cli=portTesterClient.getClient(101L);
       assertNotNull("Client non trouvé",cli);
              
    }
    
    @Test
    public void testModifierClient101(){
    
       Client cli=portTesterClient.getClient(101L);
       assertNotNull("Client non trouvé",cli);
     
       cli.setAdrCli("Beaurains");
       portTesterClient.modifierClient(cli);
       cli=portTesterClient.getClient(101L);
       
       assertNotNull("Client non trouvé",cli);
       assertEquals("MAJ Non effectuée ", "Beaurains", cli.getAdrCli());
       
    }
    

   @Test
    public void testAjouterClient(){
    
       Region r=portTesterClient.getRegion("NPDC");
      
       
       assertNotNull("Region non trouvée", r);
       
       Client cli= new FabEntiteClient().creerEntiteClient(5555L, "Leblond Fabien", "Lens", r);
       
       portTesterClient.ajouterClient(cli);
       
       
       Client cli1=portTesterClient.getClient(5555L);
       assertNotNull("Client non trouvé",cli1);
    
    }
    
    public void testSupprimerClient(){
    
        Client cli=portTesterClient.getClient(5555L);
        assertNotNull("Client non trouvé",cli); 
        
        portTesterClient.supprimerClient(cli);
        
        Client cli1=portTesterClient.getClient(5555L);
        assertNull("Client non non supprimé",cli1);
        
    }
   
    private static String reinitialiserBase() {
        ServiceInitBase service = new ServiceInitBase();
        ServiceSoapInitBase port = service.getServiceSoapInitBasePort();
        return port.reinitialiserBase();
    }
    
}