
package fabrique.entite.client;

import webservice.soap.pourtest.client.Client;
import webservice.soap.pourtest.client.Region;



/**
 *
 * @author rsmon
 */


public class FabEntiteClient {
     
    public Client creerEntiteClient(Long numCli,String nomCli, String adrCli, Region laRegion) {
        
        Client client= new Client();  
         
        client.setNumCli(numCli);
        client.setNomCli(nomCli);
        client.setAdrCli(adrCli);
      
        client.setLaRegion(laRegion);
        
        return client;
    } 
}
